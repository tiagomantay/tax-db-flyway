# tax-api

Tax-api Database migrations

# Executing

## Verifying migrations:
`mvn clean compile flyway:info`

## Applying changes:
`mvn clean compile flyway:migrate`

# Profiles
## Examples:

`mvn clean compile flyway:info -P devint -Dflyway.password=taxapipassw`

* Applying migrations locally:

`mvn clean compile flyway:migrate -P local`

* Verifying local migrations:

`mvn clean compile flyway:info`

# CAUTION!!!:

To drop your LOCAL database objects:

`mvn clean compile flyway:clean`

**ATTENTION!!!** This command **SHOULD NEVER** be executed on other profiles than locally!!!! 
