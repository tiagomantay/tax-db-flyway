CREATE TABLE country(
  id          BIGSERIAL PRIMARY KEY,
  code        VARCHAR (64) NOT NULL,
  name        VARCHAR (64) NOT NULL
);

CREATE TABLE region(
  id          BIGSERIAL PRIMARY KEY,
  country_id  BIGINT REFERENCES country(id),
  name        VARCHAR (64) NOT NULL,
  code        VARCHAR (64) NOT NULL
);

CREATE TABLE tax(
  id                     BIGSERIAL PRIMARY KEY,
  country_id             BIGINT REFERENCES country(id),
  region_id              BIGINT REFERENCES region(id),
  name                   VARCHAR (256) NOT NULL,
  created_date           TIMESTAMP NOT NULL DEFAULT NOW(),
  valid_from_date        DATE,
  valid_until_date       DATE,
  deleted                BOOLEAN NOT NULL
);

CREATE TABLE rate(
  id                   BIGSERIAL PRIMARY KEY,
  tax_id               BIGINT REFERENCES tax(id),
  value                DOUBLE PRECISION not null,
  description          VARCHAR (128)
);
